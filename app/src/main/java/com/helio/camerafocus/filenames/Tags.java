package com.helio.camerafocus.filenames;

/**
 * Created by victor on 5/15/2016.
 */
public class Tags {

    /**
     * Inventory file name tag
     */
    public static final String TAG_INVENTORY_FILENAME = "${INVENTORY_FILENAME}";
}
