package com.helio.camerafocus.filenames;

/**
 * Created by victor on 5/15/2016.
 */
public class Templates {

    /**
     * Inventory filename template
     */
    public static final String TEMPLATE_INVENTORY_FILENAME = "${INVENTORY_FILENAME}.jpeg";
}
