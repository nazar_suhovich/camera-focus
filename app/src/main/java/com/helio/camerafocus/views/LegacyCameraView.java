package com.helio.camerafocus.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.helio.camerafocus.camera.CameraOrientation;
import com.helio.camerafocus.consts.AppConstants;
import com.helio.library.io.handler.FilesHandler;
import com.helio.library.utils.Utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


/**
 * LegacyCameraView - Class that opens the Camera using the old Camera API
 * and takes pictures and saves them on disk
 * <p>
 * Created by victor on 5/11/2016.
 */
public class LegacyCameraView extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = "LegacyCameraView";
    private Utilities utilities;
    private Camera mCamera;
    private SurfaceHolder surfaceHolder;

    private int focusStartX;
    private int focusStartY;
    private int focusStopX;
    private int focusStopY;

    /**
     * Legacy camera view
     * <p>
     * Class that shows camera and take pictues using the old camera API
     *
     * @param context context associated with this custom view
     */
    public LegacyCameraView(Context context) {
        super(context);
        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
        getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        setAlpha(1f);
        utilities = new Utilities(context);
    }

    /**
     * Legacy camera view
     * <p>
     * Class that shows camera and take pictues using the old camera API
     *
     * @param context context associated with this custom view
     * @param attrs   custom xml attrs for this custom view
     */
    public LegacyCameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
        utilities = new Utilities(context);
        setAlpha(1f);
    }

    /**
     * Called when the surface is created
     *
     * @param holder The SurfaceHolder whose surface is being created.
     */
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        this.surfaceHolder = holder;

        mCamera = Camera.open();
        try {
            getCamera().setPreviewDisplay(holder);
            getCamera().startPreview();
            onSurfaceChanged(getWidth(), getHeight());
        } catch (IOException e) {
            e.printStackTrace();
            getUtilities().showSimpleSnackBar(this, "Cannot open camera[" + e.getMessage() + "]");
        }

    }

    /**
     * Called when the surface is changed
     *
     * @param holder The SurfaceHolder whose surface has changed.
     * @param format The new PixelFormat of the surface.
     * @param width  The new width of the surface.
     * @param height The new height of the surface.
     */
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        onSurfaceChanged(width, height);
    }

    /**
     * Called when the surface is changed
     *
     * @param width  The new width of the surface.
     * @param height The new height of the surface.
     */
    private void onSurfaceChanged(int width, int height) {

        getCamera().setDisplayOrientation(CameraOrientation.UPRIGHT);

        int defaultPreviewWidth = getCamera().getParameters().getPreviewSize().width;
        int defaultPreviewHeight = getCamera().getParameters().getPreviewSize().height;

        Log.i(TAG, "Default w=" + defaultPreviewWidth + " h=" + defaultPreviewHeight);

        Camera.Parameters parameters = getCamera().getParameters();

        parameters.setRotation(getBestKnownDeviceOrientation(Camera.CameraInfo.CAMERA_FACING_BACK));
        parameters.setJpegQuality(100); //Best Quality
        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
        parameters.setSceneMode(Camera.Parameters.SCENE_MODE_ACTION);
        parameters.setAntibanding(Camera.Parameters.ANTIBANDING_AUTO);
        List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();
        Camera.Size optimalSize = getOptimalPreviewSize(sizes, getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);

        defaultPreviewWidth = getCamera().getParameters().getPreviewSize().width;
        defaultPreviewHeight = getCamera().getParameters().getPreviewSize().height;

        parameters.setPictureSize(optimalSize.width,optimalSize.height);
        Log.i(TAG, "Computed w=" + defaultPreviewWidth + " h=" + defaultPreviewHeight);

        getCamera().setParameters(parameters);

        getCamera().autoFocus(new Camera.AutoFocusCallback() {
            @Override
            public void onAutoFocus(boolean success, Camera camera) {
                Log.i(TAG, String.valueOf(success));
            }
        });
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) h / w;

        if (sizes == null) return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
//            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
//            if (Math.abs(size.height - targetHeight) < minDiff) {
//                optimalSize = size;
//                minDiff = Math.abs(size.height - targetHeight);
//            }
            double diff = size.height - targetHeight;
            if(diff < minDiff) {
                optimalSize = size;
                minDiff = diff;
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }


    /**
     * Determines the current device rotation and uses this data to set the best camera picture
     * and preview picture rotation that will make the camera picture and the preview picture upright
     *
     * @return the angle of rotation
     */
    private int getBestKnownDeviceOrientation(int cameraId) {

        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        int rotation = getResources().getConfiguration().orientation;
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        return 90;
    }

    /**
     * This is called immediately before a surface is being destroyed. After
     * returning from this call, you should no longer try to access this
     * surface.  If you have a rendering thread that directly accesses
     * the surface, you must ensure that thread is no longer touching the
     * Surface before returning from this function.
     *
     * @param holder The SurfaceHolder whose surface is being destroyed.
     */
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        getCamera().stopPreview();
        getCamera().release();
        mCamera = null;
    }

    /**
     * Get camera
     *
     * @return mCamera
     */
    public Camera getCamera() {
        return mCamera;
    }

    /**
     * Get utilities
     *
     * @return utilities
     */
    public Utilities getUtilities() {
        return utilities;
    }

    /**
     * Return the focused region x0
     *
     * @return focusStartX
     */
    public int getFocusStartX() {
        return focusStartX;
    }

    /**
     * Set the focused region X0
     *
     * @param focusStartX x0
     */
    public void setFocusStartX(int focusStartX) {
        this.focusStartX = focusStartX;
    }

    /**
     * Get the focused region y0
     *
     * @return focusStartY
     */
    public int getFocusStartY() {
        return focusStartY;
    }

    /**
     * Set focus start y0
     *
     * @param focusStartY y0
     */
    public void setFocusStartY(int focusStartY) {
        this.focusStartY = focusStartY;
    }

    /**
     * Get focus x1
     *
     * @return x1
     */
    public int getFocusStopX() {
        return focusStopX;
    }

    /**
     * Set focus x1
     *
     * @param focusStopX x1
     */
    public void setFocusStopX(int focusStopX) {
        this.focusStopX = focusStopX;
    }

    /**
     * Get focus stop y1
     *
     * @return y1
     */
    public int getFocusStopY() {
        return focusStopY;
    }

    /**
     * Set the focus stop y1
     *
     * @param focusStopY y1
     */
    public void setFocusStopY(int focusStopY) {
        this.focusStopY = focusStopY;
    }

    /**
     * Get the width ratio
     *
     * @param width pictureWidth
     * @return pictureWidth / width of view
     */
    private int getWidthRatio(int width) {
        return width / getWidth();
    }

    /**
     * Get the height ratio
     *
     * @param height of picture
     * @return height of picture / height of view
     */
    private int getHeightRatio(int height) {
        return height / getHeight();
    }

    /**
     * Takes a picture that is being previewed.
     *
     * @param startX focused x0
     * @param startY focused y0
     * @param stopX  focused x1
     * @param stopY  focused y1
     */
    public void takePicture(int startX, int startY, int stopX, int stopY) {

        setFocusStartX(startX);
        setFocusStartY(startY);
        setFocusStopX(stopX - stopX / 4);
        setFocusStopY(stopY - stopY / 4);

        getCamera().takePicture(shutter, raw, jpeg);
    }

    /**
     * Callback interface used to supply image data from a photo capture.
     */
    Camera.PictureCallback raw = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            Log.i(getClass().getName().toUpperCase(), "RAW");

        }
    };
    /**
     * Callback interface used to supply image data from a photo capture.
     */
    Camera.ShutterCallback shutter = new Camera.ShutterCallback() {
        @Override
        public void onShutter() {
            Log.i(getClass().getName().toUpperCase(), "SHUTTER");
        }
    };

    /**
     * Callback interface used to supply image data from a photo capture.
     */
    Camera.PictureCallback jpeg = new
            Camera.PictureCallback() {
                @Override
                public void onPictureTaken(byte[] data, Camera camera) {
                    saveCapturedPicture(data, camera);
                }
            };

    /**
     * Saves the captured picture
     *
     * @param data
     * @param camera
     */
    public void saveCapturedPicture(byte[] data, Camera camera) {
        try {

            Log.i(getClass().getName().toUpperCase(), "JPEG");
            FilesHandler filesHandler = new FilesHandler();
            File directory = new File(AppConstants.IMAGE_STORAGE_DIR);
            filesHandler.createDirectories(directory);
            Log.i(getClass().getName().toUpperCase(), "Create Directory " + String.valueOf(directory));

            FileOutputStream inventoryImageFileStream = new FileOutputStream(new File(directory,
                    getInventoryFileName() + ".jpeg"));

            Bitmap takenPicture = BitmapFactory.decodeByteArray(data, 0, data.length);

//            int inventoryImageX0 = getFocusStartX() * getWidthRatio(takenPicture.getWidth());
//            int inventoryImageY0 = getFocusStartY() * getHeightRatio(takenPicture.getHeight());
//            int inventoryImageX1 = getFocusStopX() * getWidthRatio(takenPicture.getWidth());
//            int inventoryImageY2 = getFocusStopY() * getHeightRatio(takenPicture.getHeight());

            Bitmap bitmapCropped = Bitmap.createBitmap(takenPicture);

            Paint paint = new Paint(Paint.DITHER_FLAG);
            paint.setColor(Color.WHITE);
            paint.setTextSize(72f);

            Rect textBounds = new Rect();
            paint.getTextBounds(getInventoryFileName(), 0, getInventoryFileName().length(), textBounds);

            int textWidth = textBounds.width();
            int textHeight = textBounds.height();

            Bitmap mutableWithFileName = bitmapCropped.copy(Bitmap.Config.ARGB_8888, true);
//            Canvas canvas = new Canvas(mutableWithFileName);
//            canvas.drawText(getInventoryFileName(), mutableWithFileName.getWidth() / 2 - textWidth / 2,
//                    mutableWithFileName.getHeight() - (textHeight + 20), paint);

            mutableWithFileName.compress(Bitmap.CompressFormat.PNG, 70, inventoryImageFileStream);

            recycleAllBitmaps(new Bitmap[]{takenPicture, bitmapCropped, mutableWithFileName});
            resetCameraPreview();
            resetImageCapture();

        } catch (FileNotFoundException e) {
            Log.e(TAG, e.toString());
        }
    }

    /**
     * Recycle all bitmaps now
     *
     * @param bitmaps
     */
    private void recycleAllBitmaps(Bitmap[] bitmaps) {
        for (Bitmap bitmap : bitmaps) {
            if (!bitmap.isRecycled()) bitmap.recycle();
        }
    }

    /**
     * Reset the camera previewing
     * - The android camera preview stops after taking the picture,
     * you have to reset it to continue previewing
     */
    private void resetCameraPreview() {
        getCamera().stopPreview();
        getCamera().startPreview();
        onSurfaceChanged(getWidth(), getHeight());
    }

    //Filename of the inventory image picture file captured
    private String inventoryFileName;

    /**
     * Get the inventory file name
     *
     * @return inventoryFileName
     */
    public String getInventoryFileName() {
        return inventoryFileName;
    }

    /**
     * Set the filename of the inventory image captured
     *
     * @param inventoryFileName filename of inventory image captured
     */
    public void setInventoryFileName(String inventoryFileName) {
        this.inventoryFileName = inventoryFileName;
    }


    /**
     * Sets the captured picture to null
     */
    public void resetImageCapture() {
        Log.i(TAG, "THIS IS THE ONLY STATEMENT IN THIS METHOD");
    }

}
