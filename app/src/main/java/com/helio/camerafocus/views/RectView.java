package com.helio.camerafocus.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by victor on 5/11/2016.
 */
public class RectView extends View {

    private int focusXStart;
    private int focusYStart;
    private int focusXStop;
    private int focusYStop;

    public RectView(Context context) {
        super(context);
    }

    public RectView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint linesPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        linesPaint.setColor(Color.WHITE);
        linesPaint.setStyle(Paint.Style.STROKE);
        linesPaint.setStrokeWidth(2f);

        canvas.drawARGB(100, 255, 255, 255); //Makes the canvas transparent

        drawRectangle(canvas, linesPaint);

    }

    private void drawRectangle(Canvas canvas, Paint paint) {

        float startX = getWidth() / 16;
        float startY = getHeight() / 2 - getHeight() / 4;
        float stopX = getWidth() - getWidth() / 16;
        float stopY = getHeight() / 2 + getHeight() / 4;

        setFocusXStart((int) startX);
        setFocusYStart((int) startY);
        setFocusXStop((int) stopX);
        setFocusYStop((int) stopY);

        canvas.drawRect(startX, startY, stopX, stopY, paint);
    }

    public int getFocusXStart() {
        return focusXStart;
    }

    public void setFocusXStart(int focusXStart) {
        this.focusXStart = focusXStart;
    }

    public int getFocusYStart() {
        return focusYStart;
    }

    public void setFocusYStart(int focusYStart) {
        this.focusYStart = focusYStart;
    }

    public int getFocusXStop() {
        return focusXStop;
    }

    public void setFocusXStop(int focusXStop) {
        this.focusXStop = focusXStop;
    }

    public int getFocusYStop() {
        return focusYStop;
    }

    public void setFocusYStop(int focusYStop) {
        this.focusYStop = focusYStop;
    }
}
