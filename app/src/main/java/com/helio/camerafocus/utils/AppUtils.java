package com.helio.camerafocus.utils;

import com.helio.camerafocus.filenames.Tags;
import com.helio.camerafocus.filenames.Templates;

public class AppUtils {

    public static String parseInventoryFilename(String inventoryFileName) {
        return Templates.TEMPLATE_INVENTORY_FILENAME.replace(Tags.TAG_INVENTORY_FILENAME, inventoryFileName);
    }
}
