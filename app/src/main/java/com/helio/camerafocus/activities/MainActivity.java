package com.helio.camerafocus.activities;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.view.View;
import android.widget.RelativeLayout;

import com.helio.camerafocus.R;
import com.helio.camerafocus.consts.AppConstants;
import com.helio.camerafocus.utils.AppUtils;
import com.helio.camerafocus.views.LegacyCameraView;
import com.helio.camerafocus.views.RectView;
import com.helio.library.io.handler.FilesHandler;

import java.io.File;

/**
 * Created by victor on 5/24/2016.
 */
public class MainActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "CaptureInventoryImage";

    private RelativeLayout flCameraDisplayView;
    private LegacyCameraView legacyCameraView;
//    private RectView rvFocusArea;
    private AppCompatTextView tvInventoryFilename;
    private AppCompatImageView ivCaptureImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

    private void initViews() {
//        rvFocusArea = (RectView) findViewById(R.id.fragment_capture_inventory_image_rectView_focus_area);

        legacyCameraView = (LegacyCameraView) findViewById(R.id.fragment_capture_inventory_image_legacyCameraView_camera_preview);

        flCameraDisplayView = (RelativeLayout) findViewById(R.id.fragment_capture_inventory_image_relativeLayout_camera_display);

        ivCaptureImage = (AppCompatImageView) findViewById(R.id.fragment_capture_inventory_image_appCompatImageView_capture);
        ivCaptureImage.setOnClickListener(this);
//        rvFocusArea.setDrawingCacheEnabled(true);
//        rvFocusArea.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }

    /**
     * Called when the views are clicked
     *
     * @param v clicked view
     */
    @Override
    public void onClick(View v) {

        //Called when the image view to capture picture is clicked
        if (v == ivCaptureImage) {
            capturePicture();
        }
    }

    /**
     * Append a number to the inventory file name
     *
     * @param number
     */
    private void appendInventoryName(String number) {
        if (tvInventoryFilename.getText().length() < 10) {
            tvInventoryFilename.append(number);
        } else {
            Snackbar.make(tvInventoryFilename, "Maximum file name reached", Snackbar.LENGTH_SHORT).show();
        }

    }

    /**
     * Called when the delete button is clicked to delete the last most item
     */
    private void onBackSpaceClicked() {
        String inventoryFilename = tvInventoryFilename.getText().toString();
        if (inventoryFilename.length() > 0) {
            inventoryFilename = inventoryFilename.substring(0, inventoryFilename.length() - 1);
            tvInventoryFilename.setText(inventoryFilename);
        }
    }

    /**
     * Clear the inventory file name
     */
    private void clearFilename() {
        tvInventoryFilename.setText("");
    }

    /**
     * Called when the image view to capture picture is clicked
     */
    private void capturePicture() {
        int tmp = 1;
        legacyCameraView.setInventoryFileName("123123123" + tmp++);

        if (!isInventoryFileExists(getLegacyCameraView().getInventoryFileName())) {
            captureInventoryPictureImage();
        } else {
            alertInventoryFileExists();
        }
    }

    /**
     * Creates an alert dialog to show
     */
    private void alertInventoryFileExists() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getApplicationContext());
        alertDialog.setTitle("Overwrite file");
        alertDialog.setMessage(Html.fromHtml("<p>Inventory image file with the name <b><i>"
                + getLegacyCameraView().getInventoryFileName() + "</i></b> exists. Do you want to overwrite this file?</p>"));
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                captureInventoryPictureImage();
                Snackbar.make(ivCaptureImage, "Inventory image file saved", Snackbar.LENGTH_SHORT).show();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getLegacyCameraView().resetImageCapture();
                Snackbar.make(ivCaptureImage, "Inventory image file not overwritten, image not saved", Snackbar.LENGTH_LONG).show();
            }
        });
        alertDialog.create();
        alertDialog.show();
    }


    /**
     * Get legacy camera view
     *
     * @return legacyCameraView
     */
    public LegacyCameraView getLegacyCameraView() {
        return legacyCameraView;
    }

    /**
     * Checks whether the inventory file with the passed file name exists
     *
     * @param filename name of the inventory image file to be saved
     * @return true if filename exists false otherwise
     */
    public boolean isInventoryFileExists(String filename) {

        boolean inventoryFileExists = false;

        File inventoryImageFilesDirectory = new File(AppConstants.IMAGE_STORAGE_DIR);

        String[] filesList = inventoryImageFilesDirectory.list();

        //Directory does not exists
        if (filesList == null) {
            new FilesHandler().createDirectories(inventoryImageFilesDirectory);
            return false;
        } else {
            for (String name : inventoryImageFilesDirectory.list()) {
                if (name.equalsIgnoreCase(AppUtils.parseInventoryFilename(filename))) {
                    inventoryFileExists = true;
                }
            }
            return /*inventoryFileExists*/false;
        }
    }

    /**
     * Captures the inventory picture image
     */
    private void captureInventoryPictureImage() {
        getLegacyCameraView().takePicture(
                getLegacyCameraView().getFocusStartX(),
                getLegacyCameraView().getFocusStartY(),
                getLegacyCameraView().getFocusStopX(),
                getLegacyCameraView().getFocusStopY()
        );
    }
}
