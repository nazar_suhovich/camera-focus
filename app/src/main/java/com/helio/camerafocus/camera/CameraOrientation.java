package com.helio.camerafocus.camera;

/**
 * Created by victor on 5/11/2016.
 */
public class CameraOrientation {
    /**
     * 90 Degree orientation to make the camera picture always be upright
     */
    public static final int UPRIGHT = 90;
}
