package com.helio.camerafocus.consts;

import android.os.Environment;

public class AppConstants {
    /**
     * Inventory images storage directory
     */
    public static final String IMAGE_STORAGE_DIR = Environment.getExternalStorageDirectory() + "/MarVikApps/CameraFocus/";
}
